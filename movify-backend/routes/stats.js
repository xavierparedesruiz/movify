const express = require('express');
const router = express.Router();
const mongojs = require('mongojs')
const db = mongojs('movifystats', ['stats'])

/* GET users listing. */
router.get('/', (req, res, next) => {
  db.movieStats.find().sort({visits: -1} , (err, docs) => {
    res.json(docs)
  })
});

router.post('/', (req, res, next) => {
  
  
  const { title, release_date, imdb, backdrop_path } = req.body.movieObj;
  console.log(title, release_date, imdb, backdrop_path);
  
  db.movieStats.update({imdb, title, release_date, backdrop_path}, { $inc: {visits: 1} }, {upsert: true}, (err, response) => {
    if (err) {
      console.log(err);
      res.status(400);
    }
    else {
      console.log(response);
      res.status(200);
    } 

    res.send(response);
  });

});

module.exports = router;