# Movify

Movify és una aplicació web per a cerca de bandes sonores de pel·lícules i la seva integració amb Spotify, amb una interfície senzilla, intuïtiva i accessible, a la vegada que s’automatitza la relació de dades mitjançant diferents APIs.
Altres possibilitats són: 
· La implementació de més serveis i plataformes. 
· Aprofitar les dades i interaccions dels usuaris per a millorar els algoritmes a través del data farming.

Projecte desenvolupat per: **Marc Carranza** i **Xavier Paredes**.