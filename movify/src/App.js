import React, { Component } from 'react';
import './css/normalize.css';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';

// Importació de components propis

import Main from './components/main/Main.js';
import Explorar from './components/explorar/Explorar.js';
import Pelicula from './components/pelicula/Pelicula.js';
import Estadisticas from './components/estadisticas/Estadisticas.js'
import Utilities from './utils/tmdb/Info.js';
import LoginHandler from "./components/spotify/LoginHandler";
import LogoImg from "./img/logo.png";

const utils = new Utilities();
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      API_KEY: utils.API_KEY
    }
  }
  render() {
    return (
        <Router>
            <div>
                <nav className="nav">
                    <div className='nav__wrapper'>
                        <Link to="/" className='nav__logo'><img src={LogoImg}></img></Link>
                        <ul className='nav__list'>
                            <li className='nav__element'><Link to="/" className='nav__library'>Inicio</Link></li>
                            <li className='nav__element'><Link to="/explorar" className='nav__library'>Explorar</Link></li>
                            <li className='nav__element'><Link to="/estadisticas" className='nav__library'>Estadísticas</Link></li>
                        </ul>
                    </div>
                </nav>
                <Route exact path="/" component={Main} />
                <Route exact path="/explorar/" component={Explorar} api={this.state.API_KEY} />
                <Route exact path="/pelicula/:id" component={Pelicula} />
                <Route exact path="/login/" component={LoginHandler} />
                <Route exact path="/estadisticas" component={Estadisticas} />
            </div>
        </Router>
    );
  }
}

export default App;
