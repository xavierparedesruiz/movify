import React, { Component } from 'react';
import './Estadisticas.css';

import Grafica from './Grafica.js';
import TMDB from '../../utils/tmdb/Info.js';

const Info = new TMDB();

class Estadisticas extends Component {
    constructor(props) {
        super(props)
        this.state = {
            stats: [],
            limit: 5
        }
        
    }

    componentDidMount() {
        fetch('http://movify.tk:2200/stats')
            .then(res => res.json())
            .then(data => {
                this.setState({ stats: data }, () => {
                    let wrapper = document.querySelector('.wrapper');
                    wrapper.style.background = `url(${Info.imgBaseUrl}original${this.state.stats[0].backdrop_path}) center / cover no-repeat fixed`;                    
                })
            })       
    }

    render() {
        return (
            <div className="wrapper">
                <div className="container">
                    <h1>Las películas más visitadas por nuestros usuarios: </h1>
                    <Grafica movies={this.state.stats} limit={this.state.limit} />
                </div>
            </div>
        )
    }
}

export default Estadisticas;