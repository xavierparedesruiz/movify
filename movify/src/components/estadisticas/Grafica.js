import React, { Component } from 'react'
import ChartJS from 'chart.js';


class Grafica extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: this.props.movies,
            titles: [],
            visits: [],
        }
    }

    componentWillReceiveProps(props) {
        this.setState({ movies: props.movies }, () => {
            let titles = [];
            let visits = [];

            this.state.movies.slice(0, this.props.limit).map((movie) => {
                titles.push(`${movie.title} (${movie.release_date.split('-')[0]})`);
                visits.push(movie.visits);
            })

            this.setState({ titles, visits });
        })    
    }


    componentDidUpdate() {
        const ctx = document.getElementById("stats-chart").getContext('2d');
        var chart = new ChartJS(ctx, {
            type: 'doughnut',
            data: {
                labels: this.state.titles,
                datasets: [{
                    label: 'Número de visitas',
                    data: this.state.visits,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(153, 102, 255, 0.6)',
                        'rgba(255, 159, 64, 0.6)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    labels: {
                        fontColor: '#fefefe'
                    }
                }
            }
        });
    }

    render() {
        return (
            <div className="chart-container">
                <canvas id="stats-chart" width="150" height="50"></canvas>                
            </div>
        )
    }



}

export default Grafica;
