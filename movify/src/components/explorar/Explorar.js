import React, { Component } from 'react';
import Utilities from '../../utils/tmdb/Info.js';
import './Explorar.css';
import ResultList from "../resultList.js";

const utils = new Utilities();

class Explorar extends Component {
    constructor(props) {
        let screenSize = window.matchMedia("(min-width: 600px)")
        super(props);
        this.state = {
            API_KEY: utils.API_KEY,
            baseUrl: utils.baseUrl,
            results: [],
            limit: screenSize.matches ? 18 : 8,
            filter: 'popular'
        };
        this.searchIt();

    }

    getFilterValue(event) {
        let filterValue;
        switch (event.target.value) {
            case 'populares':
                filterValue = 'popular';
                break;
            case 'taquilla':
                filterValue = 'now_playing';
                break;
            case 'mejores':
                filterValue = 'top_rated';
                break;
        }
        this.setState({filter: filterValue}, () => this.searchIt());
        
    }

    searchIt() {
        fetch(`${this.state.baseUrl}movie/${this.state.filter}?api_key=${this.state.API_KEY}&language=es-ES&page=1&include_adult=false&region=ES`)
            .then(res => res.json())
            .then(data => {
                this.setState({results: data.results});
            })
    }

    render() {
        return (
            <div className="explorar__wrapper">
                <div className="explorar__wrapper-filtro">
                    <select onChange={(e) => this.getFilterValue(e)} className="explorar__filtro">
                        <option value = "populares">Populares</option>
                        <option value = "taquilla">En taquilla</option>
                        <option value = "mejores">Mejor Valoración</option>
                    </select>
                </div>
                <div className="explorar__resultList">
                    <ResultList results={this.state.results} limit={this.state.limit} />
                </div>
            </div>
        )
    }
}


export default Explorar;