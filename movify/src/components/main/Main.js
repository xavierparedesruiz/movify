import React, { Component } from 'react';

import SearchBar from "../searchBar.js";
import ResultList from "../resultList.js";
import './Main.css'
import Utilities from "../../utils/tmdb/Info.js";
import Logo from '../../img/logo.png';

const utils = new Utilities();

class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            API_KEY: utils.API_KEY,
            searchTerm: '',
            baseUrl: utils.baseUrl,
            results: [],
            limit: 5,
            backgrounds: [
                'camara.jpg', 'cinta.jpg', 'claqueta.jpg', 'pantalla.jpg', 'teatro.jpg', 'texas.jpg'
            ]
        }
    }

    componentDidMount() {
        let index = Math.floor((Math.random() * this.state.backgrounds.length));
        let image = this.state.backgrounds[index];
        let container = document.querySelector('.main__container'); 

        
        container.style.background = `url(/img/bgs/${ image }) center / cover`;
        document.getElementById('searchbar').focus();
        setTimeout(() => {
            document.getElementById('loader').remove();
        }, 500)

    }

    searchIt(searchTerm) {
        fetch(`${this.state.baseUrl}search/movie?api_key=${this.state.API_KEY}&language=es-ES&query=${this.state.searchTerm}&page=1&include_adult=false&region=EU`)
            .then(res => res.json())
            .then(data => {
                this.setState({
                    results: data.results
                });
            })
    }

    handleSearchInput(event) {
        if(event.target.value.length !== 0) {
            event.target.classList.add('chickendinner');
            this.setState({
                searchTerm: event.target.value
            }, () => this.searchIt(this.state.searchTerm))
            
        } else {
            this.setState({searchTerm: ' '}, () => this.searchIt(this.state.searchTerm))
            event.target.classList.remove('chickendinner');
        }
    }

    renderList() {
        if (this.state.searchTerm !== '') {
            return <ResultList results={this.state.results} limit={this.state.limit}/>
        } else {
            return false;
        }
    }

    render() {
        return (
            <div className="main__container">
                <div id="loader">
                    <img src={Logo} />
                </div>
                <div className="main__wrapper">
                    <SearchBar searchTerm={this.state.searchTerm} handleSearchInput={this.handleSearchInput.bind(this)} placeholder=""/>
                    <div className="main__resultsContainer" data-scrollbar>
                        { this.renderList() }
                    </div>
                </div>

            </div>
        )
    }
}

export default Main
