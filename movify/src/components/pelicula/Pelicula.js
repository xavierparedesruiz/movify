import React, { Component } from 'react';
import Tmdb from '../../utils/tmdb/Info';
import Spotify from '../../utils/spotify/Info';
import './Pelicula.css';
import TrackList from '../spotify/trackList';
import LoginButton from '../spotify/loginButton';
import Logo from '../../img/logo.png'
const TMDB = new Tmdb();
const SPOTIFY = new Spotify();

class Pelicula extends Component {
    constructor(props) {
        super(props);
        

        this.state = {
            statsSent: false,
            albumIndex: 0,
            movie: {},
            mainContainer: {},
            songs: [
                {
                    uri: ''
                }
            ],
            spotify: {
                logged: false
            }
        };

        this.getMovie(this.props.match.params.id);
    }

    componentDidUpdate() {

        let { title, release_date, imdb_id, backdrop_path } = this.state.movie
        let movieObj = {
            title: title,
            release_date: release_date,
            imdb: imdb_id,
            backdrop_path: backdrop_path
        }

        if (!this.state.statsSent) {
            fetch('http://movify.tk:2200/stats', {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': "application/json"
                },
                body: JSON.stringify({movieObj})
                
            })
            .then((res) => {
                this.setState({statsSent: true})
            })
        }

        const loader = document.getElementById('loader');

        loader.style.opacity = 0;

        setTimeout(() => {
            loader.remove()
        }, 500)

        
    }


    getMovie(id) {
        fetch(`${TMDB.baseUrl}movie/${id}?api_key=${TMDB.API_KEY}&language=es-ES`)
            .then(res => res.json())
            .then(data => {
                this.setState({
                    movie: data,
                    mainContainer: {
                        background: `url(${TMDB.imgBaseUrl}original${data.backdrop_path}) center / cover no-repeat fixed`,
                        display: 'flex',
                        justifyContent: 'center',
                        height: '200vh'
                    }
                }, () => this.getAlbum(data.original_title));


            });
    }

    getAlbum(title) {
        fetch(`${SPOTIFY.baseUrl}search?q=${title}&type=album&market=ES`, {
            headers: {'Authorization': `Bearer ${SPOTIFY.OAUTH}`}
        })
            .then(res => res.json())
            .then(data => {
                if (data.error === undefined) {
                    this.setState({spotify: {logged: true} ,songs: data.albums.items.filter((current) => {
                        return current.album_type === 'compilation' || current.album_type === 'album';
                    })});
                }

            })
            
    }

    renderTrackList() {
        if (this.state.spotify.logged) {
            if (this.state.songs[this.state.albumIndex] != undefined) {
                if (this.state.songs[this.state.albumIndex].uri !== '') {
                    return (
                        <TrackList uri={this.state.songs[this.state.albumIndex].uri} />
                    )
                } else {
                    return <p className="errorMsg">Ha ocurrido un error. Por favor, inténtelo más tarde.</p>
                }
            } else {
                return <p className="errorMsg">No se han encontrado canciones para esta película</p>;
            }
        } else {
            return <LoginButton />;
        }
    }


    render() {
        return (
            <div className="pelicula" style={this.state.mainContainer}>
                <div id="loader">
                    <img src={Logo} />
                </div>
                <div className="pelicula__wrapper">
                    <h1 className="pelicula__title">{this.state.movie.title}</h1>
                    <div className="pelicula__info">
                        <img className="pelicula__poster" src={`${TMDB.imgBaseUrl}w185${this.state.movie.poster_path}`} alt={`Poster de la película ${this.state.movie.title}`}/>
                        <div className="pelicula__votes">
                            <p>Puntuación media <span >{this.state.movie.vote_average}</span> / <span>{this.state.movie.vote_count}</span> votos</p>
                            <p className="pelicula__overview">{this.state.movie.overview}</p>
                            <a className="pelicula__imdb" href={`https://www.imdb.com/title/${this.state.movie.imdb_id}/`}>Ver más en IMDb</a>
                        </div>
                    </div>
                    <div id="spotify">
                        { this.renderTrackList() }
                    </div>
                </div>
            </div>
        )
    }

}

export default Pelicula;