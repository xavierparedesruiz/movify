import React, { Component } from 'react';
import Link from "react-router-dom/es/Link";

class ResultItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            poster: `http://image.tmdb.org/t/p/w185${this.props.movie.poster_path}`
        }
    }
    render() {
        return (

            <Link 
            className="resultItem" 
            to={{ pathname: `/pelicula/${this.props.movie.id}`, state: {clicked: true} }} >
                { this.props.movie.poster_path !== null &&
                <img src={this.state.poster} alt={`Póster de la película ${this.props.movie.title}`} />
                }
                <div className='resultItem__data'>
                    <div className='resultItem__header'>
                        <h2 className='resultItem__title'>{this.props.movie.title}</h2>
                        <p className='resultItem__date'>{this.props.movie.release_date}</p>
                    </div>
                    <p className='resultItem__resum'>{this.props.movie.overview.substring(0, 400) + '...'}</p>
                </div>

            </Link>
         )
    }
}
 
export default ResultItem;