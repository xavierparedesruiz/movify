import React, { Component } from 'react';

import ResultItem from "./resultItem.js";



class ResultList extends Component {
    constructor(props) {
        super(props);  
    }

    RenderMovieItems() {
        if (this.props.results.length > 0) {
            return this.props.results.slice(0, this.props.limit).map(movie => {
                return <ResultItem key={movie.id} movie={movie} />;
            });
        } else {
            return <p className="error">No se han encontrado resultados para esta búsqueda</p>;
        }
    }

    render() { 
        return ( 
            <div className="resultsList">
                {this.RenderMovieItems()}
            </div>
         )
    }
}
 
export default ResultList;
