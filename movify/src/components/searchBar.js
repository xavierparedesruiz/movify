import React, { Component } from 'react';

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            placeholder: this.props.placeholder,
            searchTerm: ''
        }
    }
       

    render() { 
        return ( 
            <div className="search-bar">
                <input onChange={this.props.handleSearchInput} placeholder={this.props.placeholder} type="text" id="searchbar" name="searchTerm" value={this.props.searchTerm} />
            </div>
         )
    }
}
 
export default SearchBar;
