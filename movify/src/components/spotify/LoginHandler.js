import React, {Component} from 'react';
import Spotify from '../../utils/spotify/Info';

const SPOTIFY = new Spotify();

class LoginHandler extends Component {
    getHashValue(key) {
        let matches = window.location.hash.match(new RegExp(key+'=([^&]*)'));
        return matches ? matches[1] : null;
    }
    getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    constructor(props) {
        super(props);

        this.state = {
            token: this.getHashValue('access_token'),
            origin: window.localStorage.getItem('origin')
        };

        localStorage.setItem('spotify_oauth', this.state.token);

        setTimeout(() => {
            window.location.href = this.state.origin || '/'
        }, 500);


    }

    render() {
        return (
            <div className="loginHandler">
            </div>
        );
    }
}

export default LoginHandler;
