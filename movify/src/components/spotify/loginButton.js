import React, {Component} from 'react';
import Spotify from '../../utils/spotify/Info';
import './loginButton.css';

const SPOTIFY = new Spotify();

class LoginButton extends Component {
    constructor(props) {
        super(props);
        window.localStorage.setItem('origin', window.location.href);

    }

    render() {
        return (
            <div className="spotifyLogin">
                <a className="spotifyLogin-btn" target="_blank" href={`${SPOTIFY.AUTH_BASE}?client_id=${SPOTIFY.CLIENT_ID}&response_type=token&redirect_uri=${SPOTIFY.REDIRECT}`} >Inicia sesión con Spotify <i className="fab fa-spotify"></i></a>
            </div>
        );
    }
}

export default LoginButton;
