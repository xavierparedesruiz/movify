import React, {Component} from 'react';

class TrackItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="tracklist__item">
                <iframe src={`https://open.spotify.com/embed?uri=${this.props.uri}`} frameBorder="0" allowtransparency="true" allow="encrypted-media"/>
            </div>
        );
    }
}

export default TrackItem;
