import React, {Component} from 'react';
import Spotify from '../../utils/spotify/Info';
import TrackItem from './trackItem';

const SPOTIFY = new Spotify();

class TrackList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tracks: {
                items: []
            }
        };
        fetch(`${SPOTIFY.baseUrl}albums/${this.props.uri.split(':')[2]}/tracks`, {
            headers: {'Authorization': `Bearer ${SPOTIFY.OAUTH}`}

        })
            .then(res => res.json())
            .then(data => {
                this.setState({tracks: data});
            });



    }

    RenderTrackItems() {
        if (this.state.tracks.items.length > 0) {
            return this.state.tracks.items.map(track => {
                return <TrackItem key={track.uri} uri={track.uri} />;
            });
        } else {
            return <p className="error">No se han encontrado resultados para esta pel·lícula</p>;
        }
    }

    render() {
        return (
            <div id="tracklist">
                {this.RenderTrackItems()}
            </div>
        );
    }
}

export default TrackList;
