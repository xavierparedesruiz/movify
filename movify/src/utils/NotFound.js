import React, { Component } from 'react';
import Link from "react-router-dom/es/Link";

class NotFound extends Component {
    constructor() {
        super();
    }
    render() {
        return (
            <div>
                <h1>404</h1>
                <p>La página que estás buscando no existe, por favor comprueba que la dirección sea correcta.</p>
                <Link to="/">Volver a la página de inicio</Link>
            </div>
        )
    }
}

export default NotFound;