class Info {
    constructor() {
        // https://accounts.spotify.com/authorize?client_id=23e6e43b8a84414394c108ebb3cdc777&redirect_uri=http://google.es&response_type=code
        this.REDIRECT = 'http://www.movify.tk/login';
        this.POST_BASE = 'https://accounts.spotify.com/api/token';
        this.AUTH_BASE = 'https://accounts.spotify.com/authorize';
        this.CLIENT_ID = '23e6e43b8a84414394c108ebb3cdc777';
        this.CLIENT_SECRET = '714815a39d0e444a9d65df49304f8cf4';
        this.baseUrl = 'https://api.spotify.com/v1/';
        this.OAUTH = localStorage.getItem('spotify_oauth') || null;
    }

    getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    setOAUTH(token) {
        this.OAUTH = token;
    }

    getID() {
        return this.CLIENT_ID;
    }

    getOAUTH() {
        return this.OAUTH;
    }

    getSECRET() {
        return this.CLIENT_SECRET
    }
    getBaseURL() {
        return this.baseUrl;
    }



}

export default Info
