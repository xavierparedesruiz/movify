class Info {
    constructor() {
        this.API_KEY = 'b8155cd0848f8282bc8bd8994b48e6d0';
        this.baseUrl = 'https://api.themoviedb.org/3/';
        this.imgBaseUrl = 'http://image.tmdb.org/t/p/';
    }

    getAPI() {
        return this.API_KEY;
    }

    getBaseURL() {
        return this.baseUrl;
    }

}

export default Info